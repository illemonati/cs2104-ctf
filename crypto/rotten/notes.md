# ROTTen

rot cipher, can be interpreted as an affine cipher, crack with https://ciphertools.co.uk/decode.php

```txt
Key (from cipher to plain text):    1n + 16
Key (from plain to cipher text):    1n + 10
```

flag is simpleciphersarepasttheirprime
