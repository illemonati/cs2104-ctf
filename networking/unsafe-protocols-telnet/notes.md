# Unsafe Protocols - Telnet

convo is

```txt
hello world
do
you
know
what
tel
net
is?
it
is an
old old
protocol
where the
flag is
hmmm
should I tell?
I dont
know
let me think a bit
maybe I should let you stwe
stew
My bad, sorry about the misspelling
Here is the flag, sorry for making you wait
ZmxhZ3t0ZWxOZXRXZWxsTmV0RGVsTmV0Q2hlbGxOZXRGYWlsTmV0fQo=
Oh wait, that is a non standard encoding
Hope you can figure that uout
*out
If not, oh well
```

ZmxhZ3t0ZWxOZXRXZWxsTmV0RGVsTmV0Q2hlbGxOZXRGYWlsTmV0fQo= is base64
decodes to flag{telNetWellNetDelNetChellNetFailNet}

flag is telNetWellNetDelNetChellNetFailNet
