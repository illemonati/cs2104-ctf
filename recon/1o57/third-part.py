data = {}

with open('third-part.txt') as file:
    for line in file.readlines():
        a, b = line.strip().split(' ')
        data[int(a)] = b

print(data)
print(' '.join(data.values()))

woods = [16, 22, 34, 36, 46, 56 ]

data2 = {}

for k, v in data.items():
    if k not in woods:
        data2[k] = v

print(data2)
print(' '.join(data2.values()))

data3 = {}

for k, v in data2.items():
    if v not in data3.values():
        data3[k] = v

print(data3)
print(' '.join(data3.values()))


perrin = [3, 0, 2, 3, 2, 5, 5, 7, 10, 12, 17, 22, 29, 39, 51, 68, 90]

data4 = {}
for k, v in data.items():
    if k in perrin or k not in woods:
        data4[k] = v

print(data4)
print(' '.join(data4.values()))

data5 = {}

for k, v in data4.items():
    if v not in data5.values():
        data5[k] = v

print(data5)
print(' '.join(data5.values()))


potato = "32:35:31:41:53:45:43:52:45:54"
potato_pages = [2, 3, 5, 7, 10, 12, 17, 22, 33, 39]


print([data[i] if i in data else None for i in potato_pages])